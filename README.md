# Abstract

Recent advances in DNA sequencing are providing increasingly accurate phylogenetic trees to study, but understanding the evolutionary forces at play in different contexts remains a huge challenge. To tackle this issue, we applied an Bayesian approach to an existing model of phenotypic and species diversification [1] in order to retrieve 11 underlying parameters (such as basal speciation and extinction rates, but also competition strength) from phylogenetic trees with known traits values at the tips.

[1] L. Aristide and H. Morlon.   Understanding the effect of competition during evolutionary radiations: an integrated model of phenotypic and species diversification. *Ecology Letters*, 22(12):2006–2017, Dec. 2019. ISSN 1461-023X, 1461-0248. [doi](https://doi.org/10.1111/ele.13385)

# Project

Most macroevolutionary models focus either on species diversification or on trait evolution, so very few can help in understanding phenomena at the interface. However, a recent work by Leandro Aristide and Hélène Morlon [1] filled this gap by developing a model of phenotypic diversification under competition pressure. Through time-discrete stochastic simulations (see below), they uncovered competition as a driving force for macroevolutionary change.
In continuity, it would now be interesting to confront this model with empirical trees. The goal is to estimate the underlying rates and competition strengths from an observed tree, which could not be achieved by manually testing some parameters and checking whether it looks like the simulated trees. Consequently, in order to correctly recover those parameters with the best possible approximation, a rigorous inference method has to be implemented.

In this work, realized under the supervision of L. Aristide and H. Morlon, we show how to infer parameters from observed trees and trait distributions with the **Approximate Bayesian Computation (ABC)** inference approach, based on numerous simulations performed thanks to an improved implementation of Aristide's one in [RPANDA](https://github.com/hmorlon/PANDA).

<img src="Images/tree_example.png" alt="Simulated tree" width="500"/>

Please look at the [our report](Computational_Biology_Project_Luiselli_Andreoletti.pdf) if you want a more detailed explanation of our methods and a presentation of our preliminary results.

Most results have been by obtained in the [main notebook](main.Rmd), from which the you can call the other functions.