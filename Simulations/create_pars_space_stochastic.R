#############################################################################################################################
## Create a parameter space for the simulation by drawing the 11 parameters from exponential distributions nb_sim times
## Creation - jeremy.andreoletti@ens.fr - 11/2019
#############################################################################################################################



create_pars_space_stochastic <- function(pars, nb_sim, k = 4, distrib = "exp", seed = sample(-10000:10000, 1)){
  
  set.seed(seed)
  
  pars_space <- data.frame(matrix(data = NA, nrow = nb_sim, ncol = 11))
  names(pars_space) <- names(pars)
  
  for (p_name in names(pars)){
    p_i = as.numeric(pars[p_name])
    if (distrib == "exp"){
      pars_space[p_name] <- data.frame(rexp(nb_sim, rate = 1/(k*p_i)))        # Exponential distribution
    } else if (distrib == "unif"){
      pars_space[p_name] <- data.frame(runif(nb_sim, min = 0, max = k*p_i))   # Uniform distribution
    } else{
      print (paste("Unknown distribution : '", distrib, "' (only 'exp' and 'unif' accepted)", sep=""))
    }
  }
  return (pars_space)
}

# # Specific distributions for each parameter
# 
# create_pars_space_stochastic <- function(pars){
#   
#   pars_space <- data.frame(NA)
#   
#   # lambda1 = 0.25
#   
#   n <- pars[lambda1,]$n
#   
#   if (n==1){p_vec <- data.frame(pars[lambda1,]$p_i)}
#   
#   else{p_vec <- data.frame(rlnorm(n, meanlog=-1, sd=1))}
#   
#   names(p_vec) <- lambda1
#   
#   pars_space <- merge(pars_space, p_vec)
#   
#   # tau0 = 0.01
#   
#   n <- pars[tau0,]$n
#   
#   if (n==1){p_vec <- data.frame(pars[tau0,]$p_i)}
#   
#   else{p_vec <- data.frame(seq(pars[tau0,]$p_min, pars[tau0,]$p_max, len=n))}
#   
#   names(p_vec) <- tau0
#   
#   pars_space <- merge(pars_space, p_vec)
#   
#   # beta = 0.6
#   
#   n <- pars[beta,]$n
#   
#   if (n==1){p_vec <- data.frame(pars[beta,]$p_i)}
#   
#   else{p_vec <- data.frame(seq(pars[beta,]$p_min, pars[beta,]$p_max, len=n))}
#   
#   names(p_vec) <- beta
#   
#   pars_space <- merge(pars_space, p_vec)
#   
#   # mu0 = 0.5
#   
#   n <- pars[mu0,]$n
#   
#   if (n==1){p_vec <- data.frame(pars[mu0,]$p_i)}
#   
#   else{p_vec <- data.frame(seq(pars[mu0,]$p_min, pars[mu0,]$p_max, len=n))}
#   
#   names(p_vec) <- mu0
#   
#   pars_space <- merge(pars_space, p_vec)
#   
#   # mubg = 0.01
#   
#   n <- pars[mubg,]$n
#   
#   if (n==1){p_vec <- data.frame(pars[mubg,]$p_i)}
#   
#   else{p_vec <- data.frame(seq(pars[mubg,]$p_min, pars[mubg,]$p_max, len=n))}
#   
#   names(p_vec) <- mubg
#   
#   pars_space <- merge(pars_space, p_vec)
#   
#   # mui0 = 0.8
#   
#   n <- pars[mui0,]$n
#   
#   if (n==1){p_vec <- data.frame(pars[mui0,]$p_i)}
#   
#   else{p_vec <- data.frame(seq(pars[mui0,]$p_min, pars[mui0,]$p_max, len=n))}
#   
#   names(p_vec) <- mui0
#   
#   pars_space <- merge(pars_space, p_vec)
#   
#   # muibg = 0.02
#   
#   n <- pars[muibg,]$n
#   
#   if (n==1){p_vec <- data.frame(pars[muibg,]$p_i)}
#   
#   else{p_vec <- data.frame(seq(pars[muibg,]$p_min, pars[muibg,]$p_max, len=n))}
#   
#   names(p_vec) <- muibg
#   
#   pars_space <- merge(pars_space, p_vec)
#   
#   # alpha1 = 0.04
#   
#   n <- pars[alpha1,]$n
#   
#   if (n==1){p_vec <- data.frame(pars[alpha1,]$p_i)}
#   
#   else{p_vec <- data.frame(seq(pars[alpha1,]$p_min, pars[alpha1,]$p_max, len=n))}
#   
#   names(p_vec) <- alpha1
#   
#   pars_space <- merge(pars_space, p_vec)
#   
#   # alpha2 = 0.04
#   
#   n <- pars[alpha2,]$n
#   
#   if (n==1){p_vec <- data.frame(pars[alpha2,]$p_i)}
#   
#   else{p_vec <- data.frame(seq(pars[alpha2,]$p_min, pars[alpha2,]$p_max, len=n))}
#   
#   names(p_vec) <- alpha2
#   
#   pars_space <- merge(pars_space, p_vec)
#   
#   # sig2 = 0.5
#   
#   n <- pars[sig2,]$n
#   
#   if (n==1){p_vec <- data.frame(pars[sig2,]$p_i)}
#   
#   else{p_vec <- data.frame(seq(pars[sig2,]$p_min, pars[sig2,]$p_max, len=n))}
#   
#   names(p_vec) <- sig2
#   
#   pars_space <- merge(pars_space, p_vec)
#   
#   # m = 20
#   
#   n <- pars[m,]$n
#   
#   if (n==1){p_vec <- data.frame(pars[m,]$p_i)}
#   
#   else{p_vec <- data.frame(seq(pars[m,]$p_min, pars[m,]$p_max, len=n))}
#   
#   names(p_vec) <- m
#   
#   pars_space <- merge(pars_space, p_vec)
#   
#   
#   for (p_name in rownames(pars)){
#     
#     n <- pars[p_name,]$n
#     
#     if (n==1){p_vec <- data.frame(pars[p_name,]$p_i)}
#     
#     else{p_vec <- data.frame(seq(pars[p_name,]$p_min, pars[p_name,]$p_max, len=n))}
#     
#     names(p_vec) <- p_name
#     
#     pars_space <- merge(pars_space, p_vec)
#     
#   }
#   
#   return (pars_space[,-1])
#   
# }


# ## Compare distributions
# 
# lambda1 = 0.25
# tau0 = 0.01
# beta = 0.6
# mu0 = 0.5
# mubg = 0.01
# mui0 = 0.8
# muibg = 0.02
# alpha1 = alpha2 = 0.04
# sig2 = 0.5
# m = 20
# 
# nb_stats = 87
# nb_sim = 10**6
# 
# pars <- data.frame(p_i = c(lambda1, tau0, beta, mu0, mubg, mui0, muibg, alpha1, alpha2, sig2, m),
#                    row.names = c("lambda1", "tau0", "beta", "mu0", "mubg", "mui0", "muibg", "alpha1", "alpha2", "sig2", "m")
# )
# 
# ## Uniform distribution
# 
# par(mfrow=c(3,4))
# for (p_name in rownames(pars)){
# 
#   p_i <- pars[p_name,]
# 
#   s <- seq(-0.1*p_i*5, 1.1*p_i*5, length.out = 100)
#   plot(s, dunif(s, min = 0, max = pars[p_name,]*5), type="l", xlab = p_name, ylab = "Densité", main = paste(p_name, "unif. prior"))
#   points(pars[p_name,], 0, col = "red", pch = 16)
# }
# 
# ## Lognormal distribution
# 
# par(mfrow=c(3,4))
# for (p_name in rownames(pars)){
# 
#   p_i <- pars[p_name,]
# 
#   s <- seq(-0.1*p_i*5, 1.1*p_i*5, length.out = 100)
#   plot(s,  dlnorm(s, meanlog = log(p_i), sdlog = 1), type="l", xlab = p_name, ylab = "Densité", main = paste(p_name, "lognorm. prior"))
#   points(p_i, 0, col = "red", pch = 16)
# }
# 
# ## Exponential distribution
# 
# par(mfrow=c(3,4))
# for (p_name in rownames(pars)){
# 
#   p_i <- pars[p_name,]
# 
#   s <- seq(-0.1*p_i*5, 1.1*p_i*20, length.out = 100)
#   plot(s,  dexp(s, rate = 1/(4*p_i)), type="l", xlab = p_name, ylab = "Densité", main = paste(p_name, "exp prior"))
#   points(p_i, 0, col = "red", pch = 16)
# }
# 
# ## Gamma distribution
# 
# par(mfrow=c(3,4))
# for (p_name in rownames(pars)){
# 
#   p_i <- pars[p_name,]
# 
#   s <- seq(-0.1*p_i*5, 1.1*p_i*5, length.out = 100)
#   k <- 2.5   # Gamma distribution shape
#   plot(s,  dgamma(s, shape = k, rate = k/p_i), type="l", xlab = p_name, ylab = "Densité", main = paste(p_name, "Gamma prior"))
#   points(p_i, 0, col = "red", pch = 16)
# }

# Look into less parameters : NO exctinction so far (set all rates to 0 and do not explore them)
# Maybe remove the competition to see if it fits
# Or fix everything except alpha ? -> Existing sim from Leandro