#include <math.h>
#include <vector>
#include <list>
#include <random>
#include <stdlib.h>
#include <Rcpp.h>

#include <iostream>

int sign(double x){
	return 1-2*signbit(x);
	// acts as sign function in R: return 1 if positive and -1 if negative
};

using namespace Rcpp;

// [[Rcpp::export]]
List C_sim_MCBD( NumericVector pars, double root_value, double age_max, double step_size,	NumericVector bounds, double seed, double max_active_lineages)
	{
	// Receive parameters for the run
	double   lambda1 	= pars[0];
	double   tau0 		= pars[1];
	double   beta 		= pars[2];
	double   mu0 		  = pars[3];
	double   mubg 		= pars[4];
	double   mui0 		= pars[5];
	double   muibg 		= pars[6];
	double   alpha1 	= pars[7];
	double   alpha2 	= pars[8];
	double   sig2 		= pars[9];
	double   m 			  = pars[10];

	// creates "matrix" to fill : use vectors of vectors
	std::vector< std::vector<double> > traits_vec;
	std::vector< std::vector<double> > lineages;

	double s = sqrt (sig2 * step_size);
	m = m * step_size;


////// Test on length of bounds -> To keep in R !//////////////////////


	bool process_dead = 0;

	traits_vec.push_back({0.0, 1.0, 1.0, root_value});
	traits_vec.push_back({1.0, -1.0, 0.0, root_value});
	//TRAITS : lineage number, status (incipient=-1/good=1/extinct=-2), sister lineage, trait values 
	lineages.push_back({0.0, -1.0, 0.0, -1.0, 1.0, 0.0, 0.0});
	lineages.push_back({0.0, -1.0, 0.0, -1.0, -1.0, 0.0, NA_REAL});
	//LINEAGES : parental node, descendant node, starting t, ending t (still alive=-1), status, sp completion/extinction t, sp completion t

	std::vector<int> actives_lineages = {0,1};
	int n_lineages = actives_lineages.size();
	int n_good = 1;
	int step_count = 1;
	double t = 0 + step_size;


//// CREATE RANDOM NUMBER GENERATOR ///
	if (seed==0) {
		seed = time(NULL);
	}
    std::mt19937 gen{seed};
    std::normal_distribution<> dnorm{0,s};
	// Use : dnorm(gen)

  	std::uniform_real_distribution<double> dunif(0.0,1.0);
  	// Use : dunif(gen)


///// Begin of the step by step simulation /////
	while (age_max - t > -step_size/2 && n_good < max_active_lineages){ 
	//#t must go one step beyond age.max to ensure simulation of last step. /2 is to avoid numerical precission issues
	// Compute trait differences among lineages
		double mat_diff[n_lineages][n_lineages];
		for (int i=0; i<n_lineages; i++){
			for (int j=0; j<n_lineages; j++){
				if (i==j) { mat_diff[i][j] = 0 ; } // NULL used in R 
				else { 
					mat_diff[i][j]=
						traits_vec.at(actives_lineages[i]).at(2+step_count) - traits_vec.at(actives_lineages[j]).at(2+step_count); 
					}
			} // If keep only last value: constant 3 instead of  2+step_count
		}
	// Created distance matrix

	//Create sign matrix
		int diff_sign[n_lineages][n_lineages];
		for (int i=0; i<n_lineages; i++){
			for (int j=0; j<n_lineages; j++){
				if (i==j) { diff_sign[i][j] = 0; } 
				else { diff_sign[i][j] = sign(mat_diff[i][j]);
							// We want 1 for positive and -1 for negative
						if (mat_diff[i][j] == 0) { 
						//if there are any equal lineages, assigns random and opposing repulsion directions
							int n = dunif(gen)<0.5;
							if (n) {diff_sign[i][j] = 1; diff_sign[j][i] = -1;}
							else {diff_sign[i][j] = -1; diff_sign[j][i] = 1;}
					}
				}
			} 
		}
	// Created sign matrix



// traits_vec loop
// There is a namespace in Rcpp called "traits"
// We use "traits_vec" to avoid conflicts or confusion
		double diff_me[lineages.size()][n_lineages-1];
		for (int i=0; i<n_lineages; i++){
			int signs_me[n_lineages];
			for (int j=0;j<n_lineages;j++) {

				signs_me[j] = diff_sign[i][j]; 
				if (j<i) {
					diff_me[actives_lineages[i]][j] = mat_diff[i][j];
				}
				else if (j>i) {
					diff_me[actives_lineages[i]][j-1] = mat_diff[i][j];
				}
			}

			// saves diff vector to use later for extinction rates
			double dist_bound[2] = {traits_vec.at(actives_lineages[i])[2+step_count] - bounds[0], traits_vec.at(actives_lineages[i])[2+step_count] - bounds[1]};
				// distance to bounds

			double bound_effect = 3*(sign(dist_bound[0]) * exp(-2*pow(dist_bound[0],2)) + sign(dist_bound[1]) * exp(-2*pow(dist_bound[1],2)));
			// repulsion of bounds

			//Update trait value
			double summed = 0;
			for (int j=0;j<n_lineages-1;j++) {
				summed = summed + signs_me[j] * exp(-alpha2 * pow(diff_me[actives_lineages[i]][j],2));
				}
			traits_vec.at(actives_lineages[i]).emplace_back(
				traits_vec.at(actives_lineages[i])[2+step_count] + alpha2*m*summed + dnorm(gen) + bound_effect);
		}
// Ended traits_vec loop	

		std::vector<double> dead_lin;
		std::vector<double> born_lin;


// Lineages branching loop
		for (int i=0; i<actives_lineages.size();i++){
			// Warning : i in R is directly the active lineage ! here I have to use actives_lieages[i] -> transform it to lin
			int lin = actives_lineages[i]; 
			// If lineage is incipient
			if (lineages.at(lin)[4] == -1){
				
				// Captures the case where sister lineage speciates again before finishing another i_sp
				double diff_trait_isp;
				if (traits_vec.at(traits_vec.at(lin)[2])[2+step_count] == NA_REAL){
					diff_trait_isp = abs(traits_vec.at(lin)[2+step_count] - traits_vec.at(traits_vec.at(lin)[2])[3+step_count]);
				}
				else { diff_trait_isp = abs(traits_vec.at(lin)[2+step_count] - traits_vec.at(traits_vec.at(lin)[2])[2+step_count]); }

				//sp completion rate depends on distance w/parent
				double lambda2 = tau0 * exp(beta * pow(diff_trait_isp,2));
				//extinction rate depends on distance with all linages, same as trait evolution
				double summed = 0;
				for (int j=0;j<n_lineages;j++) {
					summed = summed + exp(-alpha1 * pow(diff_me[lin][j],2));
				}
				double mui = alpha1 * mui0	* summed + muibg;
				// Mu for incipient species

				std::vector<double> probs_i = {lambda2/(mui + lambda2), mui/(mui+lambda2)};
				
				double anc_node;

				if (dunif(gen) <= (lambda2 + mui)* step_size){
					int event = 2;
					if (dunif(gen)<probs_i[0]){ event=1; }
					if (event == 1){
						//Speciation completion
						lineages.at(lin)[4] = 1;
						lineages.at(lin)[5] = t;
						lineages.at(lin)[6] = t;
						traits_vec.at(lin)[1] = 1;
						// std::cout << "LINEAGE "<<lin<<" COMPLETED SPECIATION\n";
					}
					else { //if event == 2
						//Extinction
						lineages.at(lin)[3] = t;
						lineages.at(lin)[4] = -2;
						lineages.at(lin)[5] = t;
						traits_vec.at(lin)[1] = -2;
						dead_lin.emplace_back(lin);

						// i sister has now a new sister: its most closely related living good sp (if any, otherwise takes closest incipient)
						double i_sister = traits_vec.at(lin)[2];
						// to find it... take first ancestral node, any descendent living lineage? if fails, take second ancestral node, an so on. if many lineages, pick random.
						for (int j=0;j<lineages.size();j++){
							if (lineages.at(j)[1] == lineages.at(i_sister)[0]) { anc_node = lineages.at(j)[0];
								break; }
						}

					while (1) { 
						// collect all living tips from focal anc_node
						std::vector<double> possible;
						for (int j=0;j<n_lineages;j++){
							if (lineages.at(actives_lineages[j])[0] >= anc_node)
									{ possible.emplace_back(actives_lineages[j]); }	
						}
						for (int j=0;j<possible.size();j++){
							if (possible[j]==i_sister || lineages.at(possible[j])[4]==2) {
								possible.erase(possible.begin()+j);
							} // not myself and not extinct at t.
						}

						if (possible.size()==0) {break;}
						//discard lineages that don't descend from anc_node
						
						for (int j=0;j<possible.size();j++){
							double lng = possible[j];
							double node_parent = lineages.at(lng)[0];
							while (1) {
								if (node_parent==anc_node) {break;}
								else if (node_parent<anc_node){
									possible.erase(remove(possible.begin(),possible.end(),lng),possible.end());
									break;
								}
								for (int k=0;k<lineages.size();k++){
									if (lineages.at(k)[1] == node_parent) { node_parent = lineages.at(k)[0];
										break; }
								}
							}
						}
						if (possible.size()>0){
							std::vector<double> goods;
							for (int j=0;j<possible.size();j++){
								if (lineages.at(possible[j])[4]==1) {
									goods.emplace_back(possible[j]);
								}
							}
							//if there are good lineages, choose goods first (randomly if there are many)
							if (goods.size()>0) {
								traits_vec.at(i_sister)[2] = goods[floor(dunif(gen)*goods.size())];
							}
							else {
								traits_vec.at(i_sister)[2] = possible[floor(dunif(gen)*possible.size())];
							}
							break;
						}//found!
						//otherwise move one node backwards and repeat
						for (int k=0;k<lineages.size();k++){
									if (lineages.at(k)[1] == anc_node) { anc_node = lineages.at(k)[0]; 
										break; }
							}
						}
					}
				}
			}


			// if lineage is good
			if (lineages.at(lin)[4] == 1) {
				// extinction rate depends on distance with all lineages, same function as trait evolution
				double summed = 0;
				for (int j=0;j<n_lineages-1;j++){
				  //diff_me size : [total lineages][actives lineages -1]
					summed += exp(-alpha1 * pow(diff_me[lin][j],2));
				}
				double mu = alpha1 * mu0 * summed + mubg;

				//captures the case when there's only one lineage alive & extinction is activated
				if (mu0!=0 && mu==0){
					mu = 0.02 * mu0;
					//when alone, a lineage has basal extinction rate (equal to having infinite distance with neighbors)
				}
				std::vector<double> probs = {lambda1/(lambda1+mu),mu/(lambda1+mu)};
				if (dunif(gen) <= (mu+lambda1) * step_size) {
					int event = 2;
					if (dunif(gen)<probs[0]){ event=1; }
					if (event == 1){
					// Speciation
						int max = 0;
						for (int j=0;j<lineages.size();j++){
							if (lineages.at(j)[0] >= max) {
								max = lineages.at(j)[0];
							}
						}
						lineages.at(lin)[1] = max +1;
						lineages.at(lin)[3] = t;

						std::vector<double> new_lin1 =
							{lineages.at(lin)[1],-1,t,-1,1,t,t};
						std::vector<double> new_lin2 =
							{lineages.at(lin)[1],-1,t,-1,-1,NA_REAL,NA_REAL};
						lineages.emplace_back(new_lin1);
						lineages.emplace_back(new_lin2);

						dead_lin.emplace_back(lin);
						double n1 = lineages.size()-2;
						double n2 = lineages.size()-1;
						born_lin.emplace_back(n1);
						born_lin.emplace_back(n2);

						std::vector<double> trait1 = {n1,1,n2};
						trait1.insert(trait1.end(), step_count, NA_REAL);
						trait1.emplace_back(traits_vec.at(lin)[3+step_count]);
						traits_vec.emplace_back(trait1);

						std::vector<double> trait2 = {n2,-1,n1};
						trait2.insert(trait2.end(), step_count, NA_REAL);
						trait2.emplace_back(traits_vec.at(lin)[3+step_count]);
						traits_vec.emplace_back(trait2);

						//has already descendent incipient lineages?
						std::vector<double> sisters;
						for (int j=0;j<traits_vec.size();j++) {
							if (traits_vec.at(j)[2]==lin && lineages.at(j)[1]==-1) { // keep only living
								sisters.emplace_back(j); 
							}
						}
						if (sisters.size() >0) {
							//update sister number of all i_sp lineages descending from this parental good lineage (or it's ancestors) with new id
							for (int j=0;j<sisters.size();j++) {
								traits_vec.at(sisters[j])[2] = lineages.size()-1
							;}
						}
					}
					// event ==2
					// extinction
					else {
						lineages.at(lin)[3] = t;
						lineages.at(lin)[4] = -2;
						lineages.at(lin)[5] = t;
						traits_vec.at(lin)[1] = -2;
						dead_lin.emplace_back(lin);

						//if sister is incipient & alive, becomes good
						double i_sister = traits_vec.at(lin)[2];
						if (lineages.at(i_sister)[4]==-1) {
							lineages.at(i_sister)[4] = 1;
							lineages.at(i_sister)[5] = t;
							lineages.at(i_sister)[6] = t;
							traits_vec.at(i_sister)[1] = -1;
						}
						// i_sister inherits older sisters from the extinct
						std::vector<double> sisters;
						for (int j=0;j<traits_vec.size();j++) { //get older lineages who have i as sister
							if (traits_vec.at(j)[2]==lin
								&& j != i_sister
								&& lineages.at(j)[4] != -2
								&& lineages.at(j)[1]== -1
								) { //discard current sister, extinct, and ancestral lineages
								sisters.emplace_back(j);
							}
						}
						for (int j=0;j<sisters.size();j++) {
							traits_vec.at(sisters[j])[2] = i_sister;
						}
						//i_sister has new sister, which is the youngest of the older non-extinct branches of i (if any left)
						if (sisters.size()>0) {
							double max = 0;
							for (int k=0;k<sisters.size();k++){
								if (sisters[k]>=max) { max = sisters[k]; }
							}
							traits_vec.at(i_sister)[2] = max;
						}
					}
				}
			}
		}

		for (int j=0;j<dead_lin.size();j++){
			actives_lineages.erase(remove(actives_lineages.begin(),actives_lineages.end(),dead_lin[j]),actives_lineages.end());
		}
		for (int j=0;j<born_lin.size();j++){
		actives_lineages.emplace_back(born_lin[j]);
		}
		step_count++;
		t += step_size;
		n_lineages = actives_lineages.size();

		
		if (n_lineages==0){
			std::cout<<"process died";
			process_dead = 1;
			break;
		}
		n_good = 0;
		for (int i=0; i<actives_lineages.size(); i++){
			int lin = actives_lineages.at(i);
			if (traits_vec.at(lin)[1]==1){
				n_good++;
			  // Seems different from the tree because a species which is extinct at time t will appear on the tree at time t and not afterwards,
			  // but would not be counted
			}
		}
	}
t -= step_size;

// Add one to all lineages numbers -> Conversion for R
// traits : lineage number, status (incipient=-1/good=1/extinct=-2), sister lineage, trait values 
// lineages: parental node, descendant node, starting t, ending t (still alive=-1), status, sp completion/extinction t, sp completion t

for (int i = 0; i<lineages.size(); i++) {

	traits_vec[i][0] += 1;
	traits_vec[i][2] += 1;
	lineages[i][0] += 1;
	lineages[i][1] += 1;
}

for (int i = 0; i<actives_lineages.size(); i++) {
	actives_lineages[i] += 1;
}

	return List::create(
		_["traits"] = traits_vec,
		_["lineages"] = lineages,
		_["active_lineages"] = actives_lineages,
		_["t"] = t,
		_["process_dead"] = process_dead
		);


};