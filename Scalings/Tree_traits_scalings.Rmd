---
title: "Scaling the trees : time and traits"
output:
  html_document:
    df_print: paged
editor_options:
  chunk_output_type: inline
---

Creation - jeremy.andreoletti@ens.fr - 11/2019

```{r, include=F}
knitr::opts_chunk$set(echo = TRUE, include=TRUE)
```

The goal of this notebook is to show with mathematical and simulation arguments that it is possible to **scale the parameters** of sim_MCBD in order to obtain scaled trees for **any given time or trait scalings**. Then I explain how we can use those scalings to do **ABC inference** from an arbitrary tree compared to centered, normalized and time-scaled simulations.

NB : Some parts of the code are not shown for readibility reasons.

# Function : sim_MCBD

```{r, include=F, message=FALSE, warning=FALSE}
source("../Summary_statistics/extract_stats.R")
# call : extract_stats(tree, traits)

source("../Simulations/sim_MCBD.R")
# call :  TODO

library('abctools')
library('castor')
library('ape')
library('RPANDA')
library('apTreeshape')
library('phytools')
library('moments')
library('dplyr')
library('tidyr')
library("reshape2")

plotSimu_normalize_t <- function(traitmat, linmat, step_size=0.01, ylims=NULL, age.max.norm=1){
  #plots a simulation, with incipient lineages in red, good in black
  step_size_corrected <- step_size*age.max.norm
  steps <- max(linmat[,4])/step_size_corrected
  max_trait <- NULL
  min_trait <- NULL
  for (i in 1:nrow(linmat)){#find extremes for plot limits
    max_trait <- c(max_trait, max(traitmat[[i]][-(1:3)], na.rm = T))
    min_trait <- c(min_trait, min(traitmat[[i]][-(1:3)], na.rm = T))
  }
  if (is.null(ylims)){
    plot(1, type="n", xlim=c(1,steps), ylim = c(min(min_trait),max(max_trait)), ylab = "trait value",
         xlab = "Time")
  }
  else{plot(1, type="n", xlim=c(1,steps), ylim = ylims, ylab = "trait value", xlab = "Time")}
  completion <- linmat[,6]
  #handle and plot each possibility
  for (i in 1:length(completion)){
    if (is.na(completion[i])){
      #extinct incipient lineages
      lines(x = (4:length(traitmat[[i]]))/age.max.norm,
            y = traitmat[[i]][4:length(traitmat[[i]])], col="red", cex=0.5)
    }
    else if (linmat[i,5] == -2 && !is.na(linmat[i,7]) && completion[i]!= linmat[i,7]){
      #extinct good lineages
      lines(x = (4:((linmat[i,7]/step_size_corrected)+4))/age.max.norm,
            y = traitmat[[i]][4:((linmat[i,7]/step_size_corrected)+4)], col="red", cex=0.5)
      lines(x = (((linmat[i,7]/step_size_corrected)+4):length(traitmat[[i]]))/age.max.norm,
            y = traitmat[[i]][((linmat[i,7]/step_size_corrected)+4):length(traitmat[[i]])], col="black", cex=0.5)
    }
    else{
      #living good and incipient lineages

      lines(x = (4:(completion[i]/step_size+4))/age.max.norm,
            y = traitmat[[i]][4:((completion[i]/step_size)+4)], col="red", cex=0.5)
      
      if(length(traitmat[[i]])>(round(completion[i]/step_size_corrected)+4)){

        lines(x = ((completion[i]/step_size+4):length(traitmat[[i]]))/age.max.norm,
              y = traitmat[[i]][((completion[i]/step_size)+4):length(traitmat[[i]])], col="black", cex=0.5)
      }
    }
  }
}
```

I put all extinction parameters to 0 in order to have more reproductible trees and not worry about whole tree extinction errors.

```{r}
lambda1 = 0.25
tau0 = 0.01
beta = 0.6
mu0 = 0 # 0.5
mubg = 0 # 0.01
mui0 = 0 # 0.8
muibg = 0 # 0.02
alpha1 = alpha2 = 0.04
sig2 = 0.5
m = 20

pars <- c(lambda1, tau0, beta, mu0, mubg, mui0, muibg, alpha1, alpha2, sig2, m)

nb_stats = 87
```

# Equivalence between time/traits tree scalings and parameters scalings

We take as a reference a given set of parameters (pars), with a root.age of 0 and an age.max of 10.

```{r, fig.width=9, fig.height=3, results="hide"}
par(mfrow=c(1,3))

for (i in 1:3){
  res <- sim_MCBD(pars, age.max = 10, plot = T)
  title("No transform")
}
```

## Time scaling

We can then *increase age.max*, and get **equivalent trees** with the initial age.max by *scaling the parameters*.

```{r}
scale_time <- function(pars, k_t){
  p <- data.frame(t(data.frame(row.names = c("lambda1", "tau0", "beta", "mu0", "mubg", "mui0", "muibg", "alpha1", "alpha2", "sig2", "m"), values = pars)))
  return (c(p$lambda1*k_t,    # Time-dependant rate
            p$tau0*k_t,       # Time-dependant rate
            p$beta,
            p$mu0*k_t,        # Time-dependant rate
            p$mubg*k_t,       # Time-dependant rate
            p$mui0*k_t,       # Time-dependant rate
            p$muibg*k_t,      # Time-dependant rate
            p$alpha1,
            p$alpha2,
            p$sig2*k_t,       # Brownian motion : 1
            p$m*k_t))         # Deterministic competition component : 2
}
```

Demonstration : introduce a time scaling parameter $k_t$ and express new scaled parameters according to $k_t$

  1. $\sigma^2 \to k_t \times \sigma^2$ :
  
  $$\delta \sim \sigma^2d(k_t.t) = (k_t\times\sigma^2)dt$$

  2. $m \to k_t \times m$ : 
  
  $$m.\alpha_2\Big[\sum_{j\neq i}^n sign\big(x_i(t)-x_j(t)\big)\times e^{\displaystyle-\alpha_2(x_i(t)-x_j(t))^2}\Big]d(k_t.t) = (k_t\times m).\alpha_2\Big[\sum_{j\neq i}^n sign\big(x_i(t)-x_j(t)\big)\times e^{\displaystyle-\alpha_2(x_i(t)-x_j(t))^2}\Big]dt$$

  3. $\lambda$, $\tau$ and $\mu$ parameters are time-dependant rate that can be scaled directly by $k_t$

```{r, fig.width=12, fig.height=6, results="hide"}
par(mfrow=c(2,4))

k_t = 2

for (i in 1:4){
  res <- sim_MCBD(pars, age.max = 10*k_t, plot = T)
  title(paste("Time scaling t x", k_t))
}

for (i in 1:4){
  res <- sim_MCBD(scale_time(pars, k_t), age.max = 10, plot = T)
  title(paste("Time scaling, k_t =", k_t))
}
```

Here are the summary statistics for 100 trees obtained with more time or scaled parameters, the distributions seem identical (confirmed with t.tests). Those figures have been generated with the `Test_time_scaling.R` script.

```{r, include=F}
knitr::include_graphics("../Images/sum_stats_scaled_time_pars.png")
```

```{r, include=F}
knitr::include_graphics("../Images/p_values_scaled_time_pars.svg")
```

## Traits scaling

We can then *multiply the trait values*, and get **equivalent trees** by *scaling the parameters*.

```{r}
scale_traits <- function(pars, k_x){
  p <- data.frame(t(data.frame(row.names = c("lambda1", "tau0", "beta", "mu0", "mubg", "mui0", "muibg", "alpha1", "alpha2", "sig2", "m"), values = pars)))
  return (c(p$lambda1,
            p$tau0,
            p$beta/k_x**2,
            p$mu0*k_x**2,
            p$mubg,
            p$mui0*k_x**2,
            p$muibg,
            p$alpha1/k_x**2,
            p$alpha2/k_x**2,
            p$sig2*k_x**2,
            p$m*k_x**3))
}
```

Demonstration : introduce a trait scaling parameter $k_x$ and express new scaled parameters according to $k_x$
$$(1)\quad x' = k_x\times x \\
\implies \lambda_{2i}(t) = \tau_0 e^{\displaystyle \beta \big(k_xx_i(t)-k_xx_j(t)\big)^2}\\
\Longleftrightarrow \lambda_{2i}(t) = \tau_0 e^{\displaystyle (k_x^2\beta) \big(x_i(t)-x_j(t)\big)^2}\\
\Longleftrightarrow \begin{cases}
                      \tau_0'       \quad = \quad \tau_0 \\\\
                      \beta'       \quad = \quad k_x^2\times\beta
                    \end{cases}
\Longleftrightarrow \begin{cases}
                      \tau_0       \quad = \quad \tau_0' \\\\
                      \beta       \quad = \quad \displaystyle\frac{\beta'}{k_x^2}
                    \end{cases}$$

$$(2)\quad x' = k_x\times x \\
\implies \mu_i = \alpha_1.\mu_0.\sum_{j\neq i}^n e^{\displaystyle - \alpha_1\Big[\big(k_x.x_i(t)-k_x.x_j(t)\big)^2\Big]}\\
\Longleftrightarrow  \mu_i = (k_x^2.\alpha_1).\frac{\mu_0}{k_x^2}.\sum_{j\neq i}^n e^{\displaystyle - (k_x^2.\alpha_1)\big(x_i(t)-x_j(t)\big)^2}\\
\Longleftrightarrow \begin{cases}
                      \alpha_1'       \quad = \quad k_x^2\times\alpha_1 \\\\
                      \mu_0'       \quad = \quad \displaystyle\frac{\mu_0}{k_x^2} \\\\
                      \mu_i'       \quad = \quad \mu_i  \\\\
                      \mu_{bg}'       \quad = \quad \mu_{bg} 
                    \end{cases}
\Longleftrightarrow \begin{cases}
                      \alpha_1       \quad = \quad \displaystyle\frac{\alpha_1'}{k_x^2} \\\\
                      \mu_0       \quad = \quad k_x^2\times\mu_0' \\\\
                      \mu_i       \quad = \quad \mu_i'  \\\\
                      \mu_{bg}       \quad = \quad \mu_{bg}' 
                    \end{cases}$$
                    
$$(3)\quad x' = k_x\times x \\
\implies k_xx_i(t+dt) = k_xx_i(t) + m\alpha_2\Big[\sum_{j\neq i}^n sign\big(k_xx_i(t)-k_xx_j(t)\big)\times e^{\displaystyle-\alpha_2(k_xx_i(t)-k_xx_j(t))^2}\Big]dt + \delta \\
\Longleftrightarrow x_i(t+dt) = x_i(t) + \frac m{k_x^3}(k_x^2.\alpha_2)\Big[\sum_{j\neq i}^n sign\big(x_i(t)-x_j(t)\big)\times e^{\displaystyle-(k_x^2.\alpha_2)(x_i(t)-x_j(t))^2}\Big]dt + \frac\delta{k_x} \\
\Longleftrightarrow \begin{cases}
                      \alpha_2'       \quad = \quad k_x^2\times\alpha_2 \\\\
                      m'       \quad = \quad \displaystyle\frac m{k_x^3} \\\\
                      \delta'       \quad = \quad \displaystyle\frac\delta{k_x}
                    \end{cases}
\Longleftrightarrow \begin{cases}
                      \alpha_2       \quad = \quad \displaystyle\frac{\alpha_2'}{k_x^2} \\\\
                      m       \quad = \quad m' \times k_x^3 \\\\
                      \delta       \quad = \quad \delta' \times k_x \to \sigma = \sigma' \times k_x \to \sigma^2 = {\sigma'}^2 \times k_x^2 \;?
                    \end{cases}$$

```{r, fig.width=12, fig.height=6, results="hide"}
par(mfrow=c(2,4))

k_x = 3

for (i in 1:4){
  res <- sim_MCBD(pars, age.max = 20, full.sim = T, plot = F)
  trait_mat_transform_lin <- sapply(res$all$trait_mat, function(x){x*k_x})
  plotSimu_normalize_t(trait_mat_transform_lin, res$all$lin_mat, age.max.norm=1)
  title(paste("Traits scaling x *", k_x))
}

for (i in 1:4){
  k_x = 3
  res <- sim_MCBD(scale_traits(pars, k_x), age.max = 20, plot = T)
  title(paste("Traits scaling k_x =", k_x))
}
```

Here are the summary statistics for 500 trees obtained with expanded traits or scaled parameters, the distributions seem identical (confirmed with t.tests). Those figures have been generated with the `Test_traits_scaling.R` script.

```{r, include=F}
knitr::include_graphics("../Images/sum_stats_scaled_traits_pars.png")
```

```{r, include=F}
knitr::include_graphics("../Images/p_values_scaled_traits_pars.svg")
```

## Simultaneous time + trait scalings

```{r, results="hide"}
res <- sim_MCBD(pars, root.value = 300, age.max = 20, full.sim = T, plot = F)
trait_mat_transform_lin <- sapply(res$all$trait_mat, function(x){(x-300)*3}+300)
plotSimu_normalize_t(trait_mat_transform_lin, res$all$lin_mat, age.max.norm=1)
title("Root 300, time scaling t*2, traits scaling x*3")
```

I will use this type of more realist double-scaled tree to test the transformations in the following part

# Center-reduce-normalize for ABC

## Test the transformations

The whole traits matrix is centered and reduced according the the extant tips only (what we have access in reality).

```{r}
center_reduce <- function(traits_matrix, tip_traits){
  return (sapply(traits_matrix, function(x)(x-mean(tip_traits))/sd(tip_traits)))
}
```

Example :

  1. Simulate a tree with scaled time and traits
  2. Center and reduce the traits
  3. Scale the time to a unite length (indicated at 100 time steps of length 0.01)
  4. Simulate equivalent trees with the modified parameters

```{r, results="hide"}
par(mfrow=c(2,3))
age.max = 10

res <- sim_MCBD(pars, root.value = 300, age.max = age.max, full.sim = T, plot = F)
trait_mat_transform_lin <- sapply(res$all$trait_mat, function(x){(x-300)*3}+300)
pars_traits_scaled <- scale_traits(pars, k_x = 3)
plotSimu_normalize_t(trait_mat_transform_lin, res$all$lin_mat, age.max.norm=1)
title("Root 300, time t*10, traits x*3")

pars_centered_reduced <- scale_traits(pars_traits_scaled, k_x = 1/sd(res$gsp_extant$tips*3))
plotSimu_normalize_t(center_reduce(trait_mat_transform_lin, res$gsp_extant$tips), res$all$lin_mat)
title("Centered reduced")

pars_time_scaled <- scale_time(pars = pars_centered_reduced, k_t = age.max)
tree_time_scaled <- res$gsp_extant$tree
tree_time_scaled$edge.length <- tree_time_scaled$edge.length/age.max
plotSimu_normalize_t(center_reduce(trait_mat_transform_lin, res$gsp_extant$tips), res$all$lin_mat, age.max.norm=age.max)
title("Centered reduced + time scaled")

res_traits_scaled <- sim_MCBD(pars_traits_scaled, root.value = 300, age.max = age.max)
title("Equivalent scaled parameters")
res_traits_scaled_centered_reduced <- sim_MCBD(pars_centered_reduced, age.max = age.max)
title("Equivalent scaled parameters")
res_traits_scaled_centered_reduced_time_scaled <- sim_MCBD(pars_time_scaled, age.max = 1)
title("Equivalent scaled parameters")
```

## How to do ABC with this ?

Steps :

1. Make a lot of simulations : draw the parameters from prior distributions
  
2. Scale all simulations :
  
    - Traits : centered + reduced (`center_reduce` function) $\to$ change the parameters accordingly (`scale_traits` function)
    - Time : normalized to a unit length (divide `edge.length` by `age.max`) $\to$ change the parameters accordingly (`scale_time` function)
    <br/><br/>
3. Scale the observed tree :
 
    - Traits + time scaling
    - Keep in memory the scaling parameters (`sd(traits)` and `age.max`)
    <br/><br/>
4. ABC :
  
    - Compare the `observed_scaled_tree` to the `simulated_scaled_tree` : summary statistics
    - Remove trees of different shapes
    - Make posterior distributions with the scaled parameters of the remaining trees
    - Scale back the posterior distributions to the observed tree units with `sd(traits)` and `age.max`

